package ru.bombimbom.googlesearch.api

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query
import ru.bombimbom.googlesearch.clean.domain.db.entity.SearchDTO
import ru.bombimbom.googlesearch.clean.domain.db.entity.SearchListResult

const val API_KEY = "AIzaSyAsNF_3G_Yrw94ZhvlNTWjCmuM-7oZz1RA"
const val API_CX = "008882912487361937813:zhinox7ag_u"

interface Api {

    @GET ("v1?/")
    suspend fun startSearch(
        @Query("key") key: String,
        @Query("cx") cx: String,
        @Query("q") queryText: String) : SearchListResult

}