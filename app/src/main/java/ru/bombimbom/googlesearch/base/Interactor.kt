package ru.bombimbom.googlesearch.base

interface Interactor<T: InteractorOut> : Cancellation {

    fun setupInteractorOut(out: T)
}