package ru.bombimbom.googlesearch

import android.app.Application
import ru.bombimbom.googlesearch.di.AppComponent
import ru.bombimbom.googlesearch.di.DaggerAppComponent
import ru.bombimbom.googlesearch.di.StorageModule
import ru.bombimbom.googlesearch.di.UtilModule

class GoogleApp : Application() {

    lateinit var appComponent : AppComponent

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent
            .builder()
            .storageModule(StorageModule(this))
            .utilModule(UtilModule(this))
            .build()

    }

}