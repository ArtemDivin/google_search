package ru.bombimbom.googlesearch.clean.data

import kotlinx.coroutines.CoroutineScope
import ru.bombimbom.googlesearch.base.CoroutineScopeImpl

abstract  class RepositoryImpl : Repository, CoroutineScopeImpl()