package ru.bombimbom.googlesearch.clean.view.adapters.sectionedadapter;

import android.view.View;

import androidx.annotation.LayoutRes;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public abstract class BaseSection<VH extends RecyclerView.ViewHolder> {

    public enum Click {INSTANCE;}

    private DataObservable dataObservable;

    private int offset = 0;
    private boolean show = true;

    @LayoutRes
    public abstract int getLayoutId();

    protected abstract VH createViewHolder(View view);

    protected abstract void bindViewHolder(VH viewHolder, int position, List<Object> payloads);

    public abstract int getSize();

    final protected int getInternalSize() {
        return show ? getSize() : 0;
    }

    final protected void registerObservable(DataObservable dataObservable) {
        if (dataObservable != null) {
            this.dataObservable = dataObservable;
        }
    }

    final void notifyItemRangeInserted(int position, int count) {
        if (dataObservable != null) {
            dataObservable.onItemRangeInserted(offset + position, count);
        }
    }

    final void notifyItemRangeRemoved(int position, int count) {
        if (dataObservable != null) {
            dataObservable.onItemRangeRemoved(offset + position, count);
        }
    }

    final void notifyItemMoved(int fromPosition, int toPosition) {
        if (dataObservable != null) {
            dataObservable.onItemMoved(offset + fromPosition, offset + toPosition);
        }
    }

    final void notifyItemRangeChanged(int position, int count, Object payload) {
        if (dataObservable != null) {
            dataObservable.onItemRangeChanged(offset + position, count, payload);
        }
    }

    final protected void setOffset(int offset) {
        this.offset = offset;
    }

    final public int getOffset() {
        return offset;
    }

    final public int getAdapterPositionForViewHolder(VH viewHolder) {
        int pos = viewHolder.getAdapterPosition();
        return (pos != RecyclerView.NO_POSITION ? pos - offset : RecyclerView.NO_POSITION);
    }

    final public int getLayoutPositionForViewHolder(VH viewHolder) {
        return viewHolder.getLayoutPosition() - offset;
    }

    public final void show(boolean show) {
        show(show, false);
    }

    protected final void show(boolean show, boolean needUpdate) {
        boolean prev = this.show;
        this.show = show;

        if (dataObservable != null) { // if not registered yet
            if (prev && !show) {
                dataObservable.onItemRangeRemoved(offset, getSize());
            } else if (!prev && show) {
                dataObservable.onItemRangeInserted(offset, getSize());
            } else if (needUpdate) {
                dataObservable.onItemRangeChanged(offset, getSize(), null);
            }
        }
    }

    public int getLonelyHeight() {
        return dataObservable.getLonelyHeight();
    }

    public boolean isShown() {
        return show;
    }

}
