package ru.bombimbom.googlesearch.clean.view.adapters.sectionedadapter;


import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public abstract class SingleSection<T, VH extends RecyclerView.ViewHolder> extends BaseSection<VH> {

    private T data;

    public void setData(T data) {
        boolean inserted = this.data == null && data != null;
        boolean changed = this.data != null && !this.data.equals(data);

        this.data = data;

        if (isDataNullable()) {
            notifyItemRangeChanged(0, 1, null);
        } else {
            if (inserted) {
                notifyItemRangeInserted(0, 1);
            } else if (changed) {
                if (data == null) {
                    notifyItemRangeRemoved(0, 1);
                } else {
                    notifyItemRangeChanged(0, 1, null);
                }
            }
        }
    }

    @Override
    final protected void bindViewHolder(VH viewHolder, int position, List<Object> payloads) {
        bindViewHolder(viewHolder, data, payloads);
    }

    @Override
    public int getSize() {
        return isDataNullable() || data != null ? 1 : 0;
    }

    protected abstract void bindViewHolder(VH viewHolder, T data, List<Object> payloads);

    protected boolean isDataNullable() {
        return false;
    };

    public T getData() {
        return data;
    }

    public void notifySectionChanged() {
        notifyItemRangeChanged(0, 1, null);
    }

}
