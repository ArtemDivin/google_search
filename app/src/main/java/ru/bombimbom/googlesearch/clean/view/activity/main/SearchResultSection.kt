package ru.bombimbom.googlesearch.clean.view.activity.main

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_search_result.view.*
import ru.bombimbom.googlesearch.R
import ru.bombimbom.googlesearch.clean.model.entity.SearchModel
import ru.bombimbom.googlesearch.clean.view.adapters.sectionedadapter.MultiSection

class SearchResultSection : MultiSection<SearchModel, SearchResultSection.VH>() {

    override fun getLayoutId(): Int = R.layout.item_search_result

    override fun createViewHolder(view: View): VH = VH(view)

    override fun bindViewHolder(vh: VH, data: SearchModel?, position: Int, payloads: MutableList<Any>?) {
        vh.bind(data)
    }


    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView){

        fun bind(data: SearchModel?) {
            itemView.tvTitle.text = data?.title
            itemView.tvDescription.text = data?.description
            itemView.tvLink.text = data?.link
        }

    }
}