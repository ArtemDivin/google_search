package ru.bombimbom.googlesearch.clean.domain.db.dao

import androidx.room.*

@Dao
interface BaseDao<T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entity: T) : Long

    @Update
     fun update(entity: T)

    @Delete
     fun delete(entity: T)

}