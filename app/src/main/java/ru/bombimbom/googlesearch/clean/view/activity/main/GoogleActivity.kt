package ru.bombimbom.googlesearch.clean.view.activity.main

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import ru.bombimbom.googlesearch.GoogleApp
import ru.bombimbom.googlesearch.R
import ru.bombimbom.googlesearch.clean.view.adapters.sectionedadapter.SectionedAdapter
import ru.bombimbom.googlesearch.di.viewmodel.injectViewModel
import ru.bombimbom.googlesearch.ext.*
import javax.inject.Inject


class GoogleActivity : AppCompatActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var mainViewModel: MainViewModel

    val adapter: SectionedAdapter by lazy { SectionedAdapter() }
    val searchSection: SearchResultSection = SearchResultSection()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        (application as GoogleApp).appComponent.inject(this)
        mainViewModel = injectViewModel(viewModelFactory)

        observe(mainViewModel.isLoading) {
            pb.setVisible(it)
        }

        observe(mainViewModel.isError) {
            AlertDialog
                .Builder(this)
                .setMessage(R.string.error_on_loading)
                .setPositiveButton(R.string.button_ok) { dialogInterface, _ -> dialogInterface.dismiss() }
                .create()
                .show()

        }

        observe(mainViewModel.searchResult) {
            searchSection.data = it
        }

        initView()
        initAdapter()
    }

    private fun initView() {

        etSearchField.onTextChange(object : OnAfterTextChangedListener {
            override fun complete(isEmpty: Boolean) {
                btnSearch.isEnabled = !isEmpty
            }
        })
        btnSearch.setOnClickListener {
            etSearchField.hideKeyboard()
            mainViewModel.onSearchClick(etSearchField.text.toString())
        }
    }

    private fun initAdapter() {
        val llm = LinearLayoutManager(this)
        rv.layoutManager = llm
        rv.adapter = adapter

        if (!adapter.contains(searchSection)) {
            adapter.addSection(searchSection)
        }
    }
}
