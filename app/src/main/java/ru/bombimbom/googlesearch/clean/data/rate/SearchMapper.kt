package ru.bombimbom.googlesearch.clean.data.rate

import ru.bombimbom.googlesearch.clean.data.Mapper
import ru.bombimbom.googlesearch.clean.domain.db.entity.SearchDTO
import ru.bombimbom.googlesearch.clean.model.entity.SearchModel

class SearchMapper : Mapper<SearchDTO, SearchModel>() {

    override fun map(from: SearchDTO): SearchModel {
      return SearchModel(
          from.id,
          from.title,
          from.link,
          from.snippet
      )
    }
}