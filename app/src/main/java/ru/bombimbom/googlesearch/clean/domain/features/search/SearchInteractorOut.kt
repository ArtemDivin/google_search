package ru.bombimbom.googlesearch.clean.domain.features.search

import ru.bombimbom.googlesearch.base.InteractorOut
import ru.bombimbom.googlesearch.clean.model.entity.SearchModel

interface SearchInteractorOut : InteractorOut {

    fun isLoading(loading: Boolean)

    fun onLoaded(searchResult: List<SearchModel>)

    fun onError(e: Throwable)
}
