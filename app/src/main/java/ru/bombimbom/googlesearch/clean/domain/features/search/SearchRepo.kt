package ru.bombimbom.googlesearch.clean.domain.features.search

import ru.bombimbom.googlesearch.base.Repository
import ru.bombimbom.googlesearch.clean.model.entity.SearchModel

interface SearchRepo : Repository {

    suspend fun startSearch(query: String): List<SearchModel>

    suspend fun loadLastQueryResult(): List<SearchModel>


}

