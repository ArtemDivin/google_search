package ru.bombimbom.googlesearch.clean.domain.db.dao

import androidx.room.*
import kotlinx.coroutines.flow.Flow
import ru.bombimbom.googlesearch.clean.domain.db.entity.SearchDTO
import ru.bombimbom.googlesearch.clean.model.entity.SearchModel

@Dao
interface SearchDao : BaseDao<SearchDTO>{

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(search: SearchDTO)

    @Query("SELECT * FROM SearchDTO limit 10")
    fun load() : List<SearchDTO>?

    @Query("DELETE FROM SearchDTO")
    fun deleteAll()

}