package ru.bombimbom.googlesearch.clean.view.adapters.sectionedadapter;

public interface DataObservable {

    void onItemRangeChanged(int positionStart, int itemCount, Object payload);

    void onItemRangeInserted(int positionStart, int itemCount);

    void onItemRangeRemoved(int positionStart, int itemCount);

    void onItemMoved(int fromPosition, int toPosition);

    int getLonelyHeight();

}
