package ru.bombimbom.googlesearch.clean.domain.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import ru.bombimbom.googlesearch.clean.data.ResponseDTO

data class SearchListResult(

    var items: List<SearchDTO>?
)
@Entity
data class SearchDTO(

    @PrimaryKey(autoGenerate = true)
    var id: Int,
    var title: String,
    var link: String,
    var snippet: String

) : ResponseDTO()
