package ru.bombimbom.googlesearch.clean.view.adapters.sectionedadapter;


import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListUpdateCallback;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;


public abstract class MultiSection<T extends Diffable<T>, VH extends RecyclerView.ViewHolder> extends BaseSection<VH> implements
        ListUpdateCallback {

    private List<T> data;

    public void setData(List<T> data) {

        DiffCallback<T> callback = new DiffCallback<>(this.data, data);
           // Log.d("TAG", "setData: 1 ");

            DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(callback);

            this.data = data;

            diffResult.dispatchUpdatesTo(this);
    }

    @Override
    final protected void bindViewHolder(VH viewHolder, int position, List<Object> payloads) {
        bindViewHolder(viewHolder, data.get(position), position, payloads);
    }

    @Override
    public final int getSize() {
        return data != null ? data.size() : 0;
    }

    protected abstract void bindViewHolder(VH viewHolder, T data, int position, List<Object> payloads);

    @Override
    public void onInserted(int position, int count) {
        notifyItemRangeInserted(position, count);
    }

    @Override
    public void onRemoved(int position, int count) {
        notifyItemRangeRemoved(position, count);
    }

    @Override
    public void onMoved(int fromPosition, int toPosition) {
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public void onChanged(int position, int count, Object payload) {
        notifyItemRangeChanged(position, count, payload);
    }

    public T getData(int position) {
        return data.get(position);
    }

    public List<T> getData() {
        return data;
    }

}
