package ru.bombimbom.googlesearch.clean.model.entity

import ru.bombimbom.googlesearch.clean.view.adapters.sectionedadapter.Diffable

data class SearchModel(

    var id: Int = 0,
    var title : String = "",
    var link: String = "",
    var description: String = ""

) : Diffable<SearchModel> {
    override fun areItemsTheSame(other: SearchModel?): Boolean {
       return id == other?.id
    }

    override fun areContentsTheSame(other: SearchModel?): Boolean {
        return title == other?.title && link == other.link
    }
}