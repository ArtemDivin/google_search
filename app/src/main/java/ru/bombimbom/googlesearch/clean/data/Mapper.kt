package ru.bombimbom.googlesearch.clean.data

abstract class Mapper < From : ResponseDTO, To > {

    abstract fun map(from: From) : To
}
