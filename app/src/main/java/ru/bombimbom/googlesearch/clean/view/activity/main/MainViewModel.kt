package ru.bombimbom.googlesearch.clean.view.activity.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import ru.bombimbom.googlesearch.clean.domain.features.search.SearchInteractor
import ru.bombimbom.googlesearch.clean.domain.features.search.SearchInteractorOut
import ru.bombimbom.googlesearch.clean.model.entity.SearchModel
import javax.inject.Inject


class MainViewModel @Inject constructor(private val searchInteractor: SearchInteractor) : ViewModel(),
    SearchInteractorOut {

    private var loadingMutable : MutableLiveData<Boolean> = MutableLiveData()
    private var errorMutable: MutableLiveData<Boolean> = MutableLiveData()
    private var searchResultMutable: MutableLiveData<List<SearchModel>> = MutableLiveData()

            var isLoading: LiveData<Boolean> = loadingMutable
            var isError: LiveData<Boolean> = errorMutable
            var searchResult: LiveData<List<SearchModel>> = searchResultMutable

    init {
        searchInteractor.setupInteractorOut(this)
        onStartApp()
    }

    fun onSearchClick(text: String) {
        viewModelScope.launch {
            searchInteractor.startSearch(text)
        }
    }

    fun onStartApp(){
        viewModelScope.launch {
            searchInteractor.loadLastData()
        }
    }

    override fun isLoading(loading: Boolean) {
        loadingMutable.value = loading
    }

    override fun onLoaded(searchResult: List<SearchModel>) {
       searchResultMutable.value = searchResult
    }

    override fun onError(e: Throwable) {
       errorMutable.value = true
    }
}