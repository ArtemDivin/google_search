package ru.bombimbom.googlesearch.clean.domain.db

import androidx.room.Database
import androidx.room.RoomDatabase
import ru.bombimbom.googlesearch.clean.domain.db.dao.SearchDao
import ru.bombimbom.googlesearch.clean.domain.db.entity.SearchDTO

@Database(entities = [SearchDTO::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun searchDao() : SearchDao

}