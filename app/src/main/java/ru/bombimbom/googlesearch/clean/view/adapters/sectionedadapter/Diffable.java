package ru.bombimbom.googlesearch.clean.view.adapters.sectionedadapter;


public interface Diffable<T extends Diffable<T>>  {

    boolean areItemsTheSame(T other);

    boolean areContentsTheSame(T other);



}
