package ru.bombimbom.googlesearch.clean.data

import io.reactivex.functions.Cancellable
import ru.bombimbom.googlesearch.base.Cancellation

interface Repository : Cancellation