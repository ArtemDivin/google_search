package ru.bombimbom.googlesearch.clean.model.repo

import ru.bombimbom.googlesearch.api.API_CX
import ru.bombimbom.googlesearch.api.API_KEY
import ru.bombimbom.googlesearch.api.Api
import ru.bombimbom.googlesearch.clean.data.RepositoryImpl
import ru.bombimbom.googlesearch.clean.data.rate.SearchMapper
import ru.bombimbom.googlesearch.clean.domain.db.dao.SearchDao
import ru.bombimbom.googlesearch.clean.domain.features.search.SearchRepo
import ru.bombimbom.googlesearch.clean.model.entity.SearchModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SearchRepoImpl @Inject constructor(private val api: Api, private val searchDao: SearchDao) : RepositoryImpl(), SearchRepo {

    override suspend fun startSearch(query: String): List<SearchModel> = ioAsync {
             searchDao.deleteAll()
             api.startSearch(API_KEY, API_CX, query).items?.map {
                 searchDao.save(it)
                 SearchMapper().map(it)
             }?: listOf()
    }

    override suspend fun loadLastQueryResult(): List<SearchModel> = ioAsync {
        searchDao.load()?.map {
            SearchMapper().map(it)
        }?: listOf()
    }
}