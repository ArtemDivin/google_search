package ru.bombimbom.googlesearch.clean.domain.features.search

import ru.bombimbom.googlesearch.base.InteractorImpl
import javax.inject.Inject

class SearchInteractorImpl @Inject constructor(private val searchRepo: SearchRepo) :
    InteractorImpl<SearchInteractorOut>(), SearchInteractor {

    override suspend fun startSearch(query: String) {
        launchSafely(
            { out.isLoading(it) },
            { out.onError(it) }) {
            out.onLoaded(searchRepo.startSearch(query))
        }
    }

    override suspend fun loadLastData() {
        launchSafely(
            { out.isLoading(it) },
            { out.onError(it) }) {
            out.onLoaded(searchRepo.loadLastQueryResult())
        }
    }

}