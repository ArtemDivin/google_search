package ru.bombimbom.googlesearch.clean.view.adapters.sectionedadapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;


final public class SectionedAdapter extends RecyclerView.Adapter<ViewHolder> implements DataObservable {

    private final ArrayList<BaseSection> sections;
    private final TreeMap<Integer, BaseSection> map;

    private RecyclerView recyclerView;
    private int initialHeight = ViewGroup.LayoutParams.MATCH_PARENT;

    public SectionedAdapter() {
        sections = new ArrayList<>();
        map = new TreeMap<>();
    }

    public void addSection(BaseSection section) {
        if (section == null) {
            throw new NullPointerException("Section cannot be null");
        }
        if (sections.contains(section)) {
            throw new IllegalArgumentException("Section already added");
        }

        section.registerObservable(this);
        sections.add(section);
        recalculateOffsets();
    }

    @Override
    public void onAttachedToRecyclerView(final RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
        recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    recyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    recyclerView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
                initialHeight = recyclerView.getHeight();
            }
        });
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        this.recyclerView = null;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        BaseSection section = sections.get(viewType);
        View view = LayoutInflater.from(parent.getContext()).inflate(section.getLayoutId(), parent, false);
        return section.createViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Entry<Integer, BaseSection> entry = map.floorEntry(position);
        BaseSection section = entry.getValue();

        section.bindViewHolder(holder, position - entry.getKey(), null);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position, List<Object> payloads) {
        Entry<Integer, BaseSection> entry = map.floorEntry(position);
        BaseSection section = entry.getValue();

        section.bindViewHolder(holder, position - entry.getKey(), payloads);
    }

    @Override
    public int getItemViewType(int position) {
        BaseSection section = map.floorEntry(position).getValue();
        return sections.indexOf(section);
    }

    @Override
    public int getItemCount() {
        Entry<Integer, BaseSection> lastEntry = map.lastEntry();

        return lastEntry == null ? 0 : lastEntry.getKey() + lastEntry.getValue().getInternalSize();
    }

    @Nullable
    public BaseSection getSectionForPosition(int position) {
        Entry<Integer, BaseSection> entry = map.floorEntry(position);
        return entry != null ? entry.getValue() : null;
    }

    @Override
    public void onItemRangeChanged(int positionStart, int itemCount, Object payload) {
        notifyItemRangeChanged(positionStart, itemCount, payload);
    }

    @Override
    public void onItemRangeInserted(int positionStart, int itemCount) {
        recalculateOffsets();
        notifyItemRangeInserted(positionStart, itemCount);
    }

    @Override
    public void onItemRangeRemoved(int positionStart, int itemCount) {
        recalculateOffsets();
        notifyItemRangeRemoved(positionStart, itemCount);
    }

    @Override
    public void onItemMoved(int fromPosition, int toPosition) {
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public int getLonelyHeight() {
        return initialHeight - recyclerView.getTop();
    }

    private void recalculateOffsets() {
        map.clear();

        int offset = 0;
        for (BaseSection section : sections) {
            section.setOffset(offset);
            map.put(offset, section);

            offset += section.getInternalSize();
        }
    }

    public ArrayList<BaseSection> getSections() {
        return sections;
    }

    @NotNull
    public Boolean contains(@NotNull BaseSection section) {
        return sections.contains(section);
    }
}

