package ru.bombimbom.googlesearch.clean.domain.features.search

import ru.bombimbom.googlesearch.base.Interactor

interface SearchInteractor : Interactor<SearchInteractorOut> {

    suspend fun startSearch(query: String)

    suspend fun loadLastData()
}