package ru.bombimbom.googlesearch.di

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import ru.bombimbom.googlesearch.R
import javax.inject.Singleton

@Module
class UtilModule(var context: Context) {

    @Singleton
    @Provides
    fun sharedPrefers() : SharedPreferences {
        return context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE)
    }


}