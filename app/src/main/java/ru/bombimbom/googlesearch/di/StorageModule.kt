package ru.bombimbom.googlesearch.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import ru.bombimbom.googlesearch.clean.domain.db.AppDatabase
import javax.inject.Singleton

@Module
class StorageModule(val context: Context) {

    var appDatabase: AppDatabase =
        Room.databaseBuilder(context, AppDatabase::class.java, "DB")
            .fallbackToDestructiveMigration()
            .build()

    @Singleton
    @Provides
    fun providesDatabase(): AppDatabase {
        return appDatabase
    }

    @Provides
    fun providesContext(): Context {
        return context
    }

}