package ru.bombimbom.googlesearch.di

import dagger.Module
import dagger.Provides
import ru.bombimbom.googlesearch.api.Api
import ru.bombimbom.googlesearch.clean.domain.db.dao.SearchDao
import ru.bombimbom.googlesearch.clean.domain.features.search.SearchInteractor
import ru.bombimbom.googlesearch.clean.domain.features.search.SearchInteractorImpl
import ru.bombimbom.googlesearch.clean.domain.features.search.SearchRepo
import ru.bombimbom.googlesearch.clean.model.repo.SearchRepoImpl
import javax.inject.Singleton

@Module
class RateModule {

    @Singleton
    @Provides
    fun providesRateRepo(api: Api, searchDao: SearchDao) : SearchRepo = SearchRepoImpl(api, searchDao)

    @Singleton
    @Provides
    fun providesRateInteractor(rateRepo: SearchRepo) : SearchInteractor = SearchInteractorImpl(rateRepo)


}