package ru.bombimbom.googlesearch.di

import dagger.Module
import dagger.Provides
import ru.bombimbom.googlesearch.clean.domain.db.AppDatabase
import ru.bombimbom.googlesearch.clean.domain.db.dao.SearchDao

@Module
class DaoModule {


    @Provides
    fun rateDao(db: AppDatabase): SearchDao {
        return db.searchDao()
    }
}