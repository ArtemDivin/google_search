package ru.bombimbom.googlesearch.di

import dagger.Component
import ru.bombimbom.googlesearch.clean.view.activity.main.GoogleActivity
import ru.bombimbom.googlesearch.di.viewmodel.ViewModelModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    RateModule::class,
    StorageModule::class,
    UtilModule::class,
    ViewModelModule::class,
    NetworkModule::class,
    DaoModule::class])
interface AppComponent {

    fun inject(activity: GoogleActivity)


}